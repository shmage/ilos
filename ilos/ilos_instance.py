#!/usr/bin/env python3

import cv2 as cv
import numpy as nm
import subprocess
import time
import math

class ilos_instance:

    def __init__(self, animate=False, minimum=0, maximum=100):
        self.animate = animate
        self.minimum = minimum
        self.maximum = maximum

    def set_brightness(self):
        cam = cv.VideoCapture(0)

        if not cam.isOpened():
            raise Exception("Couldn't open the camera.")
        check, frame = cam.read()
        cam.release()
        dst = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        result = cv.mean(dst)[2] #cv2::mean returns hue, saturation, and value
        #print("estimated brightness: ", result)

        perc = round(result/2.56, 0)
        b = subprocess.Popen(['light'], shell=True, stdout=subprocess.PIPE)
        current = b.stdout.read().strip().decode()[0:-3]

        if perc < self.minimum:
            perc = self.minimum
        elif perc > self.maximum:
            perc = self.maximum

        if self.animate:
            self.animate_transition(int(current), int(perc))
        else:
            self.invoke_backlight(perc)

    def daemon(self, interval):
        self.set_brightness()
        while True:
            time.sleep(interval)
            self.set_brightness()

    def invoke_backlight(self, value):
        subprocess.Popen(['light', '-S', str(value)])

    def animate_transition(self, init, final):
        iterator = 1
        if init > final:
            for x in range(init, final, -1):
                self.invoke_backlight(x)
                time.sleep(0.3 / iterator)
                iterator += 0.5
        else:
            for x in range(init, final):
                self.invoke_backlight(x)
                time.sleep(0.3 / iterator)
                iterator += 0.5
