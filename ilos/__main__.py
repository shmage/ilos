#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import sys
from ilos import ilos_instance

DEFAULT_INTERVAL = 30
DEFAULT_MINIMUM = 0
DEFAULT_MAXIMUM = 100

def main():
    parser = argparse.ArgumentParser(prog='ilos')
    group = parser.add_argument_group()
    
    group.add_argument('-a', '--animate', help='If set, makes the transition smoother between brightness levels.', action="store_true")
    group.add_argument('-d', '--device', type=str, help='Sets the video device to use. Unsetting this option lets OpenCV select.')
    group.add_argument('-c', '--continuous', help='Run in continuous mode. Runs until manually stopped.', action='store_true')
    group.add_argument('-i', '--interval', type=int, help='Sets interval for brightness setting. Only valid with -c. Default is 30 seconds')
    group.add_argument('-m', '--minimum', type=int, help='Minimum brightness (0-100). Use if some brightness settings are just too dark.', default=DEFAULT_MINIMUM)
    group.add_argument('-M', '--maximum', type=int, help='Maximum brightness (0-100).', default=DEFAULT_MAXIMUM)
    args = parser.parse_args()
    
    if args.animate is not None:
        session = ilos_instance.ilos_instance(args.animate, minimum=args.minimum, maximum=args.maximum)
    else:
        session = ilos_instance.ilos_instance(minimum=args.minimum, maximum=args.maximum)

    __evaluate_arguments(args, parser, session)

def __evaluate_arguments(args, parser, session):
    if args.continuous:
        if args.interval is not None:
            session.daemon(args.interval)
            exit(0)
        else:
            session.daemon(DEFAULT_INTERVAL)
            exit(0)

    elif args.interval is not None:
        parser.print_help(sys.stderr)
        exit(0)

    else:
        session.set_brightness()

if __name__ == '__main__':
    main()
